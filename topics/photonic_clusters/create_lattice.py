import numpy as np

def hexagonal_lattice_layers(L):
    """return a hexagonal lattice with unit spacing and L layers"""
    k1 = np.array([1,0,0], dtype=float)
    k2 = np.array([np.cos(np.pi/3), np.sin(np.pi/3), 0], dtype=float)

    lattice = [np.zeros(3)]

    for step in range(1,L+1):
        lattice.append(step*k1)

        for direc in [k2-k1, -k1, -k2, -k2+k1, k1, k2]:
            for _ in range(step):
                lattice.append(lattice[-1] + direc)
        lattice.pop()

    return np.asarray(lattice, dtype=float)

def hexagonal_lattice_particles(N):
    """return a hexagonal lattice with unit spacing and N particles"""
    L = int(np.ceil(np.sqrt((N-1)/3)))
    lattice = hexagonal_lattice_layers(L)
    return lattice[:N]
