import numpy as np
from topics.core_satellite.functions import *

def visualize(spheres, title=""):
    """ Use VPython to visual core-satellite"""
    import vpython as visual
    blue = visual.color.blue
    gray = visual.color.gray

    scene = visual.canvas(background=visual.color.white, title=title, width=1000, height=1000, autocenter=False, autoscale=False, range=.3)
    rad1 = np.linalg.norm(spheres[0].pos) - spheres[0].radius
    ball = visual.sphere(pos=visual.vector(*convert((0,0,0))) , radius=rad1, color=blue)
    for sphere in spheres:
        print(sphere.radius)
        visual.sphere(pos=visual.vector(*convert(sphere.pos)), radius=sphere.radius, color=visual.vec(0.9,0.9,0.9), 
                texture=visual.textures.metal)


def pause(scene):
    import vpython as visual
    print("Press <Space> to play animation!")
    while True:
        visual.rate(50)
        if scene.mouse.events:
            m = scene.mouse.getevent()
            if m.click == 'left': return
        elif scene.kb.keys:
            k = scene.kb.getkey()
            return 


def animate(spheres, rate=10, repeat=False, title=""):
    import vpython as visual
    blue = visual.color.blue
    red = visual.color.red

    scene = visual.display(background=visual.color.white, title=title, width=1000, height=1000, autocenter=False, autoscale=False, range=.3)
    rad1 = np.linalg.norm(spheres[0].pos) - spheres[0].radius
    ball = visual.sphere(pos=convert((0,0,0)) , radius=rad1, color=blue)


    pause(scene)
    while True:
        vspheres = []
        for sphere in spheres:
            visual.rate(rate)
            vspheres.append( visual.sphere(pos=convert(sphere.pos), radius=sphere.radius, color=red) )

        if not repeat:
            break

        for vsphere in vspheres:
            vsphere.visible = False
            del vsphere

        

def potential_animate(spheres_t, rate=.1, repeat=False, title=""):
    import vpython as visual
    blue = visual.color.blue
    red = visual.color.red

    scene = visual.display(background=visual.color.white, title=title, width=1000, height=1000, autocenter=False, autoscale=False, range=.3)
    rad1 = np.linalg.norm(spheres_t[0][0].pos) - spheres_t[0][0].radius
    ball = visual.sphere(pos=convert((0,0,0)) , radius=rad1, color=blue)
    vspheres = []
    for sphere in spheres_t[0]:
        vspheres.append( visual.sphere(pos=convert(sphere.pos), radius=sphere.radius, color=red) )

    pause(scene)
    while True:
        for spheres in spheres_t[::1]:
            visual.sleep(.02)
            for i,sphere in enumerate(spheres):
                vspheres[i].pos = convert(sphere.pos)


        if not repeat:
            break

        for vsphere in vspheres:
            vsphere.visible = False
            del vsphere
