import numpy as np
import random
from tqdm import tqdm
from collections import namedtuple
from .functions import sph_to_cart
from scipy.optimize import minimize

cluster = namedtuple('Cluster', ['position', 'radius'])

def make_random_cluster(core_radius, particle_radius, N, min_sep=2e-9, max_iter=50000, center=np.array([0,0,0])):
    """Makes a claster by randomly placing spheres on the surface

    Arguments:
        core_radius       radius of the core
        particle_radius   radius of the particles
        N                 number of particles
        min_sep           minimum surface-to-surface separation
        max_iter          maximum number of attempted iterations
        center            center position of cluster

    Returns (positions[N,3], radii[N])
    """
    if np.isscalar(particle_radius):
        particle_radius = np.ones(N)*particle_radius

    center = np.asarray(center)
    positions = np.zeros([N,3], dtype=float)
    radii = np.zeros([N], dtype=float)

    total = 0
    noSuccess = 0
    with tqdm(total=N,mininterval=0) as pbar:
        pbar.write("Placing spheres randomly on the sphere")
        while total < N:
            if noSuccess > max_iter:
                pbar.write("Reached {0} failed iterations. Giving Up!".format(max_iter))
                break 

            rad = core_radius + particle_radius[total]
            theta = np.arccos(2*random.random()-1)
            phi = random.random()*2*np.pi
            pos = sph_to_cart(rad, theta, phi)

            if np.any(np.linalg.norm(positions[:total] - pos, axis=1) < (radii[:total] + particle_radius[total] + min_sep)):
                noSuccess += 1
                continue
            
            positions[total] = pos
            radii[total] = particle_radius[total]
            total += 1
            noSuccess = 0
            pbar.update(1)

    positions = np.asarray(positions[:total], dtype=float) + center
    radii = np.asarray(radii[:total], dtype=float)

    return cluster(positions, radii)

def make_fibonacci_cluster(rad1, rad2, N, randomize=False):
    """Uses a Fibonacci method to place spheres in an ordered way
           rad1         inner radius (in um)
           rad2         outer radius (in um)
           N            # of outer particles
           randomize    (bool) add element of randomness
       Returns list of spheres"""

    print("Creating cluster with fibonacci method...")
    rnd = 1.
    if randomize:
        rnd = random.random() * N

    spheres = []
    offset = 2./N
    increment = math.pi * (3. - math.sqrt(5.));

    for i in range(N):
        y = ((i * offset) - 1) + (offset / 2);
        r = math.sqrt(1 - pow(y,2))

        phi = ((i + rnd) % N) * increment

        x = math.cos(phi) * r
        z = math.sin(phi) * r
        pos = (rad1+rad2)*np.array([x,y,z])

        spheres.append(sphere_obj(pos,rad2))

    print("Finished! Placed {0} spheres on the sphere\n".format(N))
    return spheres

def make_min_potential_cluster(pos, radii, max_iter=10000): 
    print("Creating cluster with minimum potential method...")
    core_rad = np.linalg.norm(pos[0]) - radii[0]
    N = len(pos)

    pbar =  tqdm(mininterval=0.5)

    ind = np.triu_indices(N,1)
    def cost(points):
        points = points.reshape((N,2))

        a1 = np.multiply.outer(np.sin(points[:,0]), np.sin(points[:,0] ))[ind]
        a2 = np.cos( np.subtract.outer(points[:,1], points[:,1])[ind] )
        a3 = np.multiply.outer(np.cos(points[:,0]), np.cos(points[:,0]))[ind]
        
        pbar.update()

        cart_points = np.zeros((N,3))
        cart_points[:,0] = (core_rad + radii)*np.sin(points[:,0])*np.cos(points[:,1])
        cart_points[:,1] = (core_rad + radii)*np.sin(points[:,0])*np.sin(points[:,1])
        cart_points[:,2] = (core_rad + radii)*np.cos(points[:,0])

        return  np.sum( 1/(2**.5*(core_rad+np.average(radii))*(1-a1*a2-a3)**.5))

    def jacobian(points):
        points = points.reshape((N,2))

        a1 = np.multiply.outer(np.sin(points[:,0]), np.sin(points[:,0] ))
        a2 = np.cos( np.subtract.outer(points[:,1], points[:,1]) )
        a3 = np.multiply.outer(np.cos(points[:,0]), np.cos(points[:,0]))
        rij = (2**.5*(core_rad+np.average(radii))*np.sqrt((np.abs(1-a1*a2-a3))))
        
        jac = np.zeros(2*N)
        for i in range(N):
            ind = np.delete(np.arange(N),i)  
            deriv = np.sum( (-np.cos(points[i,0])*np.sin(points[ind,0])*np.cos(points[i,1] - points[ind,1]) + np.sin(points[i,0])*np.cos(points[ind,0]))/rij[i,ind]**3 )
            jac[2*i] = -(core_rad+np.average(radii))**2*deriv
        for i in range(N):
            ind = np.delete(np.arange(N),i)  
            deriv = np.sum( (np.sin(points[i,0])*np.sin(points[ind,0])*np.sin(points[i,1] - points[ind,1]))/rij[i,ind]**3 )
            jac[2*i+1] = -(core_rad+np.average(radii))**2*deriv
        return jac


    guess = np.copy(pos)
    guess_sph = np.zeros((N,2))
    guess_sph[:,1] = np.arctan2(guess[:,1],guess[:,0])
    guess_sph[:,0] = np.arccos(guess[:,2])
    bounds = [[0,np.pi], [0, 2*np.pi]]*N

    result = minimize(cost, guess_sph, method='TNC',jac=jacobian, 
                        options={'disp': False, 'minfev': 0, 'scale': None, 'rescale': -1, 'offset': None, 'gtol': -1, 'eps': 1e-03, 'eta': -1, 'maxiter': max_iter, 'maxCGit': -1, 'mesg_num': None, 'ftol': -1, 'xtol': -1, 'stepmx': 0, 'accuracy': 0})
    pbar.close()
    print("Convergence status: {0}, {1}".format(result.success, result.message))

    points = result.x.reshape((N,2)) 
    cart_points = np.zeros((N,3))
    cart_points[:,0] = (core_rad+radii)*np.sin(points[:,0])*np.cos(points[:,1])
    cart_points[:,1] = (core_rad+radii)*np.sin(points[:,0])*np.sin(points[:,1])
    cart_points[:,2] = (core_rad+radii)*np.cos(points[:,0])

    print("Finished! Placed {0} spheres on the sphere\n".format(N))
    return cart_points, radii

def make_icosphere_cluster(rad1, rad2, N):
    print("Creating cluster with icosphere method...")
    faces = icosphere(N)
    spheres = []
    points = []
    for f in faces:
        scale = (rad1+rad2)/np.linalg.norm(f.v1)
        if not any((np.linalg.norm(f.v1 - x)<2*rad2).all() for x in points):
            spheres.append (sphere_obj(scale*f.v1, rad2) )
            points.append(f.v1)
        if not any((np.linalg.norm(f.v2 - x)<2*rad2).all() for x in points):
            spheres.append (sphere_obj(scale*f.v2, rad2) )
            points.append(f.v2)
        if not any((np.linalg.norm(f.v3 - x)<2*rad2).all() for x in points):
            spheres.append (sphere_obj(scale*f.v3, rad2) )
            points.append(f.v3)
    print("Finished! Placed {0} spheres on the sphere\n".format(len(spheres)))
    return spheres


if __name__ == "__main__":
    # Cluster parameters
    rad1 = 165.0/1000   # inner radius
    rad2 = 20.0/1000    # outer radius
    N = 162             # Number of spheres

    # Make clusters
    spheres_ico = make_icosphere_cluster(rad1, rad2, 2)
    spheres_fib = make_fibonacci_cluster(rad1, rad2, N)
    spheres_fib = remove_overlap(spheres_fib)
    spheres_random = make_random_cluster(rad1, rad2, N, sep=2)
    
    spheres_overlap = make_random_cluster(rad1, rad2, N, sep=-2, maxSep=4)
    spheres_potential, spheres_t = make_min_potential_cluster(spheres_ico)

    potential_animate(spheres_t)
    # Write clusters to file
    output(spheres_random, "random.dat")
    output(spheres_fib, "fib.dat")
    output(spheres_potential, "potential.dat")
    output(spheres_ico, "ico.dat")
    output(spheres_overlap, "overlap.dat")

    # Compute and plot minimum separation distances
    minSep = -4
    maxSep = 20
    bins = 40

    # import matplotlib
    # plt.style.use('ggplot')
    # matplotlib.rc('text', usetex = True)
    # matplotlib.rc('font', **{'family' : "arial"})
    # params = {'text.latex.preamble' : [r'\usepackage{siunitx}', r'\usepackage{amsmath}']}
    # plt.rcParams.update(params)
    # matplotlib.rcParams.update({'font.size': 22})

    # sep = nearest_neighbors(spheres_random)
    # plt.hist(sep*1000, bins=bins,color='blue', alpha=0.5, label="random", range=(minSep,maxSep))
    # sep = nearest_neighbors(spheres_overlap)
    # plt.hist(sep*1000, bins=bins,color='orange', alpha=0.5, label="random (overlap)", range=(minSep,maxSep))
    # sep = nearest_neighbors(spheres_fib)
    # plt.hist(sep*1000, bins=bins,color='red', alpha=0.5, label="fibonacci", range=(minSep,maxSep))
    # sep = nearest_neighbors(spheres_potential)
    # plt.hist(sep*1000, bins=bins,color='green', alpha=0.5, label="potential energy", range=(minSep,maxSep))
    # sep = nearest_neighbors(spheres_ico)
    # plt.hist(sep*1000, bins=bins,color='purple', alpha=0.5, label="icosphere", range=(minSep,maxSep))
    # plt.tight_layout()

    
    # plt.xlabel("Nearest neighbor separation distance (nm)")
    # plt.ylabel("Nanoparticle Count")
    # plt.legend(loc=2)
    # plt.show()

    visualize(spheres_fib, title="Fibonacci")
    visualize(spheres_random, title="Random")
    visualize(spheres_overlap, title="Overlap")
    visualize(spheres_potential, title="Potential")
    visualize(spheres_ico, title="Ico")
    # animate(spheres_fib, repeat=False)



