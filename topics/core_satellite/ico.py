import numpy as np

t = (1 + 5**.5)/2
rad2 = .1

class face:
    def __init__(self, v1, v2, v3):
        self.v1 = np.array(v1)
        self.v2 = np.array(v2)
        self.v3 = np.array(v3)

    def divide(self):
        a1 = (self.v1 + self.v2)/2
        a2 = (self.v2 + self.v3)/2
        a3 = (self.v3 + self.v1)/2

        a1 *= np.linalg.norm(self.v1)/np.linalg.norm(a1)
        a2 *= np.linalg.norm(self.v2)/np.linalg.norm(a2)
        a3 *= np.linalg.norm(self.v3)/np.linalg.norm(a3)
        
        faces = []
        faces.append( face(self.v1, a1, a3) )
        faces.append( face(self.v2, a2, a1) )
        faces.append( face(self.v3, a3, a2) )
        faces.append( face(a1,a2,a3) )

        return faces

def icosahedron():
    p1 = (-1,t,0)
    p2 = (1,t,0)
    p3 = (-1,-t,0)
    p4 = (1,-t,0)
    p5 = (0, -1, t)
    p6 = (0, 1, t)
    p7 = (0, -1, -t)
    p8 = (0, 1, -t)
    p9 = (t, 0, -1)
    p10 = (t, 0, 1)
    p11 = (-t, 0, -1)
    p12 = (-t, 0, 1)

    faces = []

    faces.append( face(p1,p12,p6)  )
    faces.append( face(p1,p6,p2)   )
    faces.append( face(p1,p2,p8)   )
    faces.append( face(p1,p8,p11)  )
    faces.append( face(p1,p11,p12) )

    faces.append( face(p2,p6,p10)  )
    faces.append( face(p6,p12,p5)   )
    faces.append( face(p12,p11,p3)   )
    faces.append( face(p11,p8,p7)  )
    faces.append( face(p8,p2,p9) )

    faces.append( face(p4,p10,p5)  )
    faces.append( face(p4,p5,p3)   )
    faces.append( face(p4,p3,p7)   )
    faces.append( face(p4,p7,p9)  )
    faces.append( face(p4,p9,p10) )

    faces.append( face(p5,p10,p6)  )
    faces.append( face(p3,p5,p12)   )
    faces.append( face(p7,p3,p11)   )
    faces.append( face(p9,p7,p8)  )
    faces.append( face(p10,p9,p2) )

    return faces

def icosphere(N):
    cur_faces = icosahedron()
    for i in range(N):
        next_faces = []
        for f in cur_faces:
            next_faces.extend(f.divide())
        cur_faces = next_faces
    return cur_faces

def visualize(faces):
    scene = visual.display(background=visual.color.black, title="", width=1000, height=1000, autocenter=False, autoscale=False, range=3.5)
    for i,f in enumerate(faces):
        # color = (.5,.5,.5) if (i != 15 and i <20) else (1,0,0)
        color = (.5,.5,.5)
        visual.faces (pos = [f.v1, f.v2, f.v3] , normal = np.cross(f.v2 - f.v1, f.v3-f.v1), color=color)
        visual.curve(pos=[f.v1, f.v2, f.v3, f.v1], radius=0.005)
        # visual.sphere(pos = f.v1, radius=0.02)
        # visual.sphere(pos = f.v2, radius=0.02)
        # visual.sphere(pos = f.v3, radius=0.02)

if __name__ == "__main__":
    import vpython as visual

    faces = icosphere(0)
    # faces.extend( faces[15].divide() )

    visualize(faces)
    # faces = icosphere(1)
    # visualize(faces)
    # faces = icosphere(2)
    # visualize(faces)
    # faces = icosphere(3)
    # visualize(faces)
