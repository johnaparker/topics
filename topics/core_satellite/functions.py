import numpy as np
from copy import deepcopy
vec = np.array

# Sphere

class sphere_obj:
    """Basic sphere object"""
    def __init__(self, pos, r):
        self.pos = vec(pos)
        self.radius = r

# Conveinance functions

def convert(vec):
    return [vec[1],vec[2],vec[0]]

def unconvert(vec):
    return [vec[2],vec[0],vec[1]]

def distance_sq(v1,v2):
    return (v1[0] - v2[0])**2 + (v1[1] - v2[1])**2 + (v1[2] - v2[2])**2

def sph_to_cart(r, theta, phi, origin=None):
    """convert spherical coordinates (r, theta, phi) centered at origin to cartesian coordinates (x, y, z)"""
    if origin is None:
        origin = np.zeros(3, dtype=float)

    x = origin[0] + r*np.sin(theta)*np.cos(phi)
    y = origin[1] + r*np.sin(theta)*np.sin(phi)
    z = origin[2] + r*np.cos(theta)

    return np.array([x,y,z], dtype=float)

def output(spheres, filename):
    """output r,x,y,z data as binary file"""
    print("Outputing spheres to binary file {0}".format(filename))
    allData = []
    for s in spheres:
        x,y,z = s.pos
        r = s.radius
        allData.extend([x,y,z,r])
    allData = np.array(allData)
    allData.tofile(filename)

def remove_overlap(spheres):
    """remove any particles to prevent overlapping"""
    rad2 = spheres[0].radius
    new_spheres = deepcopy(spheres)
    points = [sphere.pos for sphere in spheres]
    ind = []
    for i,pi in enumerate(points):
        pi = points[i]
        for j,pj in enumerate(points):
            if j == i:
                continue
            dist = np.linalg.norm(pj-pi)
            if dist < 2*rad2:
                ind.append(j)
    ind = set(ind)
    ind = sorted(ind, reverse=True)
    for i in ind:
        new_spheres.pop(i)

    print("{0} particle removed".format(len(ind)))
    return new_spheres

def output_txt(spheres, filename):
    """output r,x,y,z data as text file"""
    print("Outputing spheres to text file {0}".format(filename))
    outFile = open(filename)

    rad1 = np.linalg.norm(spheres[0].pos) - spheres[0].radius
    rad2 = spheres[0].radius
    N = len(spheres)
    outFile.write("N = {0}\n".format(N))
    outFile.write("SiO2 Diameter= {0} nm\n".format(2*rad1))
    outFile.write("Nano Diameter = {0}\n\n\n".format(2*rad2))

    for s in spheres:
        x,y,z = s.pos
        outFile.write("{0}\t{1}\t{2}\n\n".format(x,y,z))
    outFile.close()        

def nearest_neighbors(spheres):
    """Determine the nearest neighbors distances"""
    points = [sphere.pos for sphere in spheres]
    rad2 = spheres[0].radius
    sep = []
    for i,pi in enumerate(points):
        pi = points[i]
        minDist = np.inf
        for j,pj in enumerate(points):
            if j == i:
                continue
            pj = points[j]
            dist = np.linalg.norm(pj-pi)
            minDist = min(minDist, dist)
        sep.append(minDist)
    
    return np.array(sep) - 2*rad2
