import numpy as np
import stoked
import miepy
from copy import deepcopy

class electrodynamics(stoked.interactions):
    def __init__(self, cluster, unpolarized=False):
        """
        Arguments:
            cluster     miepy.cluster object
        """
        self.cluster = cluster
        self.unpolarized = unpolarized

        if unpolarized:
            self.c1 = deepcopy(cluster)
            self.c1.source.polarization = [1,0]
            self.c1.source.k_stored = None

            self.c2 = deepcopy(cluster)
            self.c2.source.polarization = [0,1]
            self.c2.source.k_stored = None

    def update(self):
        pos = np.copy(self.position)

        if self.unpolarized:
            if isinstance(self.cluster, miepy.cluster):
                orientation = np.copy(self.orientation)
                self.c1.update(position=pos, orientation=orientation)
                self.c2.update(position=pos, orientation=orientation)
            else:
                self.c1.update_position(pos)
                self.c2.update_position(pos)
        else:
            if isinstance(self.cluster, miepy.cluster):
                orientation = np.copy(self.orientation)
                self.cluster.update(position=pos, orientation=orientation)
            else:
                self.cluster.update_position(pos)

    def force(self):
        if self.unpolarized:
            return (self.c1.force() + self.c2.force())/2
        else:
            return self.cluster.force()

    def torque(self):
        if self.unpolarized:
            return (self.c1.torque() + self.c2.torque())/2
        else:
            return self.cluster.torque()

def cluster_get_anisotropic_drag(cluster, viscosity):
    Nparticles = cluster.Nparticles
    radii = np.zeros([Nparticles, 3], dtype=float) 

    for i in range(Nparticles):
        p = cluster.particles[i]
        if type(p) is miepy.sphere:
            radii[i] = p.radius
        elif type(p) is miepy.core_shell:
            radii[i] = p.core_radius + p.shell_thickness
        elif type(p) is miepy.cylinder:
            radii[i] = (p.radius, p.radius, p.height)
        elif type(p) is miepy.spheroid:
            radii[i] = (p.axis_xy, p.axis_xy, p.axis_z)
        elif type(p) is miepy.ellipsoid:
            radii[i] = (p.rx, p.ry, p.rz)
        elif type(p) is miepy.sphere_cluster_particle:
            rx = np.max(p.p_position[:,0] - p.position[0]) + np.max(p.p_radii)
            ry = np.max(p.p_position[:,1] - p.position[1]) + np.max(p.p_radii)
            rz = np.max(p.p_position[:,2] - p.position[2]) + np.max(p.p_radii)
            radii[i] = (rx, ry, rz)

    drag = stoked.drag_ellipsoid(radii=radii, viscosity=viscosity)
    return drag

def cluster_get_orientation(cluster):
    Nparticles = cluster.Nparticles
    orientation = np.zeros(Nparticles, dtype=np.quaternion)
    for i in range(Nparticles):
        orientation[i] = cluster.particles[i].orientation

    return orientation

def stoked_from_cluster_2d(cluster, dt, temperature=300, viscosity=8e-4, hydrodynamic_coupling=False,
                           unpolarized=False, force=None, torque=None, interface=False, integrator=None, inertia=None):
    if cluster.interface is not None:
        interface = stoked.interface()
    elif interface:
        interface = stoked.interface()
    else:
        interface = None

    if isinstance(cluster, miepy.cluster):
        radius = [p.enclosed_radius() for p in cluster.particles]

        interactions=[stoked.collisions_sphere(radius, 1),
                      stoked.double_layer_sphere(radius=radius, potential=-77e-3, temperature=temperature),
                      electrodynamics(cluster, unpolarized)]

        position = np.copy(cluster.position)
        orientation = cluster_get_orientation(cluster)
        drag = cluster_get_anisotropic_drag(cluster, viscosity)

    else:
        radius = cluster.radius

        interactions=[stoked.collisions_sphere(radius, 1),
                      stoked.double_layer_sphere(radius=radius, potential=-77e-3, temperature=temperature),
                      electrodynamics(cluster, unpolarized)]

        position = np.copy(cluster.position)
        orientation = None
        drag = stoked.drag_sphere(radius=radius, viscosity=viscosity)

    zval = cluster.position[0,2]
    bd = stoked.stokesian_dynamics(position=position, orientation=orientation, drag=drag, temperature=temperature, dt=dt, 
                                   constraint=stoked.constrain_position(z=zval),
                                   interactions=interactions,
                                   hydrodynamic_coupling=hydrodynamic_coupling,
                                   integrator=integrator,
                                   force=force,
                                   torque=torque,
                                   interface=interface,
                                   inertia=inertia)

    return bd

def stoked_from_cluster_3d(cluster, dt, temperature=300, viscosity=8e-4, hydrodynamic_coupling=False,
                           unpolarized=False, force=None, torque=None, interface=False, integrator=None, inertia=None):
    if cluster.interface is not None:
        zpos = cluster.interface.z
    else:
        zpos = 0

    if cluster.interface is not None:
        interface = stoked.interface()
    elif interface:
        interface = stoked.interface()
    else:
        interface = None

    if isinstance(cluster, miepy.cluster):
        radius = [p.enclosed_radius() for p in cluster.particles]

        interactions=[stoked.collisions_sphere(radius, 1),
                      stoked.collisions_sphere_interface(radius, 1, zpos=zpos),
                      stoked.double_layer_sphere(radius=radius, potential=-77e-3, temperature=temperature),
                      stoked.double_layer_sphere_interface(radius=radius, potential=-77e-3, potential_interface=-75e-3, zpos=zpos),
                      # stoked.van_der_waals_sphere_interface(radius, hamaker=.5e-19, zpos=zpos),
                      electrodynamics(cluster, unpolarized)]

        position = np.copy(cluster.position)
        orientation = get_orientation(cluster)
        drag = get_anisotropic_drag(cluster, viscosity)
    else:
        radius = cluster.radius

        interactions=[stoked.collisions_sphere(radius, 1),
                      stoked.collisions_sphere_interface(radius, 1, zpos=zpos),
                      stoked.double_layer_sphere(radius=radius, potential=-77e-3, temperature=temperature),
                      stoked.double_layer_sphere_interface(radius=radius, potential=-77e-3, potential_interface=-75e-3, zpos=zpos),
                      # stoked.van_der_waals_sphere_interface(radius, hamaker=.5e-19, zpos=zpos),
                      electrodynamics(cluster, unpolarized)]

        position = np.copy(cluster.position)
        orientation = None
        drag = stoked.drag_sphere(radius=radius, viscosity=viscosity)

    bd = stoked.stokesian_dynamics(position=position, orientation=orientation, drag=drag, temperature=temperature, dt=dt, 
                                   interactions=interactions,
                                   hydrodynamic_coupling=hydrodynamic_coupling,
                                   force=force,
                                   torque=torque,
                                   integrator=integrator,
                                   interface=interface,
                                   inertia=inertia)

    return bd

def net_torque(cluster):
    """Determine the net torque acting on a cluster"""

    com = np.average(cluster.position, axis=0)
    R = cluster.position - com
    F = cluster.force()
    torque = np.cross(R, F, axis=1)

    return np.sum(torque, axis=0)
