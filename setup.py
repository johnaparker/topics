import os
from setuptools import setup, find_packages

setup(
    name = "topics",
    author = "John Parker",
    author_email = "japarker@uchicago.com",
    description = ("shared python code for topics folder"),
    packages=find_packages(),
    install_requires=['numpy'],
    include_package_data = True,
)
